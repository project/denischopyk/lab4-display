//-----------------------------------------------------------------------------
//              Драйвер ЖКИ 320х240 на основе контроллера SSD1289
//-----------------------------------------------------------------------------
#include "ssd1289.h"
#include "fonts.h"



typedef union
{
  uint16_t uint16_t;
  uint8_t uint8_t[2];
}ColorTypeDef;

//                    Примитивные функции работы с сигналами
//-----------------------------------------------------------------------------

#define LCD_Backlight_OFF()   GPIOD->BRR  |=  GPIO_BRR_BR14;      // Выключение подсветки ЖКИ
#define LCD_Backlight_ON()    GPIOD->BSRR  |=  GPIO_BSRR_BS14;     // Включение подсветки ЖКИ



//-----------------------------------------------------------------------------
//                          Внутренние функции
//-----------------------------------------------------------------------------

#define LCD_Set_RS    GPIOC->BSRR |=  GPIO_BSRR_BS8;  // Установка линии RS в 1
#define LCD_Clr_RS    GPIOC->BRR  |=  GPIO_BRR_BR8;   // Сброс линии RS в 0
#define LCD_Set_RD    GPIOD->BSRR |=  GPIO_BSRR_BS15;   // Установка линии RD в 1
#define LCD_Clr_RD    GPIOD->BRR  |=  GPIO_BRR_BR15; // Сброс линии RD в 0
#define LCD_Set_WR    GPIOC->BSRR |=  GPIO_BSRR_BS6;   // Установка линии WR в 1
#define LCD_Clr_WR    GPIOC->BRR  |=  GPIO_BRR_BR6;  // Сброс линии WR в 0
#define LCD_Set_CS    GPIOC->BSRR |=  GPIO_BSRR_BS9;   // Установка линии CS в 1
#define LCD_Clr_CS    GPIOC->BRR  |=  GPIO_BRR_BR9; // Сброс линии CS в 0


//-----------------------------------------------------------------------------
//                          Инициализация ЖКИ
//-----------------------------------------------------------------------------
void LCD_Init()
{
//разрешить тактирование GPIOD, GPIOC, GPIOE
 RCC->APB2ENR |= (RCC_APB2ENR_IOPDEN | RCC_APB2ENR_IOPCEN | RCC_APB2ENR_IOPEEN);

 // BLACKLIGHT - PD.14,
GPIOD->CRH   &= ~GPIO_CRH_MODE14;      //очистить разряды MODE
GPIOD->CRH   &= ~GPIO_CRH_CNF14;       //очистить разряды CNF
GPIOD->CRH   |=  GPIO_CRH_MODE14_1;    //выход, 2MHz
GPIOD->CRH   |=  GPIO_CRH_CNF14_0;     //общего назначения, открытый сток
 // RD - PD.15
GPIOD->CRH   &= ~GPIO_CRH_MODE15;      //очистить разряды MODE
GPIOD->CRH   &= ~GPIO_CRH_CNF15;       //очистить разряды CNF
GPIOD->CRH   |=  GPIO_CRH_MODE15;      //выход, 50MHz
GPIOD->CRH   &= ~GPIO_CRH_CNF15;       //общего назначения, симетричный
// RS -PC.8, CS - PC.9
GPIOC->CRH   &= ~GPIO_CRH_MODE8;       //очистить разряды MODE
GPIOC->CRH   &= ~GPIO_CRH_CNF8;        //очистить разряды CNF
GPIOC->CRH   |=  GPIO_CRH_MODE8;       //выход, 50MHz
GPIOC->CRH   &= ~GPIO_CRH_CNF8;        //общего назначения, симетричный

GPIOC->CRH   &= ~GPIO_CRH_MODE9;       //очистить разряды MODE
GPIOC->CRH   &= ~GPIO_CRH_CNF9;        //очистить разряды CNF
GPIOC->CRH   |=  GPIO_CRH_MODE9;       //выход, 50MHz
GPIOC->CRH   &= ~GPIO_CRH_CNF9;        //общего назначения, симетричный
// WR -PC.6
GPIOC->CRL   &= ~GPIO_CRL_MODE6;       //очистить разряды MODE
GPIOC->CRL   &= ~GPIO_CRL_CNF6;        //очистить разряды CNF
GPIOC->CRL   |=  GPIO_CRL_MODE6;       //выход, 50MHz
GPIOC->CRL   &= ~GPIO_CRL_CNF6;        //общего назначения, симетричный
// DATA - PE
GPIOE->CRL   &= ~GPIO_CRL_MODE;      //очистить разряды MODE
GPIOE->CRL   &= ~GPIO_CRL_CNF;       //очистить разряды CNF
GPIOE->CRL   |=  GPIO_CRL_MODE;      //выход, 50MHz
GPIOE->CRL   &= ~GPIO_CRL_CNF;       //общего назначения, симетричный

GPIOE->CRH   &= ~GPIO_CRH_MODE;      //очистить разряды MODE
GPIOE->CRH   &= ~GPIO_CRH_CNF;       //очистить разряды CNF
GPIOE->CRH   |=  GPIO_CRH_MODE;      //выход, 50MHz
GPIOE->CRH   &= ~GPIO_CRH_CNF;       //общего назначения, симетричный


  LCD_Set_RD;
  LCD_Set_WR;
  LCD_Set_CS;
  LCD_Set_RS;
  LCD_Backlight_ON();

  LCD_Write_REG(0x0000,0x0001);    // Запуск внутреннего генератора
  LCD_Write_REG(0x0001,0x693F);    // Нормальное положение экрана
  //LCD_Write_REG(0x0001,0x6B3F);    // Зеркально по оси X
  //LCD_Write_REG(0x0001,0x293F);    // Зеркально по оси Y
  LCD_Write_REG(0x0002,0x0600);    //
  LCD_Write_REG(0x0003,0xA8A4);    //

  LCD_Write_REG(0x0005,0x0000);    //
  LCD_Write_REG(0x0006,0x0000);    //
  LCD_Write_REG(0x0007,0x0233);    //

  LCD_Write_REG(0x000B,0x0000);    //
  LCD_Write_REG(0x000C,0x0000);    // RGB interface setting
  LCD_Write_REG(0x000D,0x080C);    //
  LCD_Write_REG(0x000E,0x2B00);    //
  LCD_Write_REG(0x000F,0x0000);    // Gate scan position
  LCD_Write_REG(0x0010,0x0000);    //
  //LCD_Write_REG(0x0011,0x4070);    //  262000 цветов
  LCD_Write_REG(0x0011,0x6070);    //  65000 цветов

  LCD_Write_REG(0x0015,0x00D0);    //
  LCD_Write_REG(0x0016,0xEF1C);    //
  LCD_Write_REG(0x0017,0x0003);    //

  LCD_Write_REG(0x001E,0x00B0);    //

  LCD_Write_REG(0x0022,0x0022);    //
  LCD_Write_REG(0x0023,0x0000);    //
  LCD_Write_REG(0x0024,0x0000);    //
  LCD_Write_REG(0x0025,0x8000);    //

  LCD_Write_REG(0x0030,0x0707);    //
  LCD_Write_REG(0x0031,0x0204);    //
  LCD_Write_REG(0x0032,0x0204);    //
  LCD_Write_REG(0x0033,0x0502);    //
  LCD_Write_REG(0x0034,0x0507);    //
  LCD_Write_REG(0x0035,0x0204);    //
  LCD_Write_REG(0x0036,0x0204);    //
  LCD_Write_REG(0x0037,0x0502);    //
  LCD_Write_REG(0x003A,0x0302);    //
  LCD_Write_REG(0x003B,0x0302);    //

  LCD_Write_REG(0x0041,0x0000);    //
  LCD_Write_REG(0x0042,0x0000);    //

  LCD_Write_REG(0x0044,0xEF00);    //
  LCD_Write_REG(0x0045,0x0000);    //
  LCD_Write_REG(0x0046,0x013F);    //

  LCD_Write_REG(0x0048,0x0000);    //
  LCD_Write_REG(0x0049,0x013F);    //
  LCD_Write_REG(0x004A,0x0000);    //
  LCD_Write_REG(0x004B,0x0000);    //

  LCD_Write_REG(0x004E,0x0000);    // X=0
  LCD_Write_REG(0x004F,0x0000);    // Y=0
}

//-----------------------------------------------------------------------------
//                      Запись данных в регистр ЖКИ
//-----------------------------------------------------------------------------
void LCD_Write_REG(uint16_t Adr, uint16_t Data)
{
  LCD_Clr_CS;                                          /*   CS\   */
  LCD_Clr_RS;                                          /*   RS\   */

  GPIOE->ODR  = Adr;        // Загрузка адреса регистра ЖКИ

  LCD_Clr_WR;                                          /*   WR\   */
  LCD_Set_WR;                                          /*   WR/   */
  LCD_Set_RS;                                          /*   RS/   */

  GPIOE->ODR  =  Data;                // Загрузка данных в регистр ЖКИ

  LCD_Clr_WR;                                          /*   WR\   */
  LCD_Set_WR;                                          /*   WR/   */
  LCD_Set_CS;                                          /*   CS/   */
}
//-----------------------------------------------------------------------------
//                     Отправка комманды ЖКИ
//-----------------------------------------------------------------------------
void LCD_Write_Command(uint8_t Comm)
{
  GPIOE->ODR  =  Comm;                // Загрузка комманды в шину ЖКИ

  LCD_Clr_CS;                                          /*   CS\   */
  LCD_Clr_RS;                                          /*   RS\   */
  LCD_Clr_WR;                                          /*   WR\   */
  LCD_Set_WR;                                          /*   WR/   */
  LCD_Set_CS;                                          /*   CS/   */
}
//-----------------------------------------------------------------------------
//                     Отправка данных ЖКИ
//-----------------------------------------------------------------------------
void LCD_Write_Data(uint16_t Data)
{
  GPIOE->ODR  =  Data;                // Загрузка данных в шину ЖКИ

  LCD_Clr_CS;                                          /*   CS\   */
  LCD_Set_RS;                                          /*   RS/   */
  LCD_Clr_WR;                                          /*   WR\   */
  LCD_Set_WR;                                          /*   WR/   */
  LCD_Set_CS;                                          /*   CS/   */
}



//-----------------------------------------------------------------------------
//                      Чтение данных из ЖКИ
//-----------------------------------------------------------------------------
uint16_t LCD_Read_Data()
{
  unsigned int Data;
  GPIOE->CRH   &= ~GPIO_CRH_MODE;      //очистить разряды MODE
  GPIOE->CRH   &= ~GPIO_CRH_CNF;       //очистить разряды CNF
  GPIOE->CRH   |=  0x44444444;     //дискретный вход, третье состояние
  GPIOE->CRL   &= ~GPIO_CRL_MODE;      //очистить разряды MODE
  GPIOE->CRL   &= ~GPIO_CRL_CNF;       //очистить разряды CNF
  GPIOE->CRL   |=  0x44444444;     //дискретный вход, третье состояние
  LCD_Clr_CS;                                          /*   CS\   */
  LCD_Set_RS;                                          /*   RS/   */
  LCD_Clr_RD;                                          /*   RD\   */

  Data = GPIOE->IDR;        // Читаем данные из ЖКИ

  LCD_Set_RD;                                          /*   RD/   */
  LCD_Set_CS;                                          /*   CS/   */

  GPIOE->CRL   &= ~GPIO_CRL_MODE;      //очистить разряды MODE
  GPIOE->CRL   &= ~GPIO_CRL_CNF;       //очистить разряды CNF
  GPIOE->CRL   |=  GPIO_CRL_MODE;      //выход, 50MHz
  GPIOE->CRL   &= ~GPIO_CRL_CNF;       //общего назначения, симетричный

  GPIOE->CRH   &= ~GPIO_CRH_MODE;      //очистить разряды MODE
  GPIOE->CRH   &= ~GPIO_CRH_CNF;       //очистить разряды CNF
  GPIOE->CRH   |=  GPIO_CRH_MODE;      //выход, 50MHz
  GPIOE->CRH   &= ~GPIO_CRH_CNF;       //общего назначения, симетричный
  return(Data);
}
//-----------------------------------------------------------------------------
//							Функция записи блока
void LCD_WriteDataMultiple(uint16_t * pData, int NumItems){
while (NumItems--) {
    LCD_Write_Data(*pData++); // = PortAPI.pfWrite16_A1
}
}
//-----------------------------------------------------------------------------
//							Функция чтения блока
void LCD_ReadDataMultiple(uint16_t * pData, int NumItems)
{
while (NumItems--)
{
	*pData++ = LCD_Read_Data;
}
}
//-----------------------------------------------------------------------------
//                      Чтение данных из регистра ЖКИ
// Adr - адрес регистра
//-----------------------------------------------------------------------------
uint16_t LCD_Read_REG(uint16_t Adr)
{
  LCD_Write_Command(Adr);                   // Запись адреса регистра в ЖКИ
  return(LCD_Read_Data());                  // Возвращаем считанные данных из регистра ЖКИ
}
//-----------------------------------------------------------------------------
//                      Установка курсора ЖКИ
// x,y - координаты курсора
//-----------------------------------------------------------------------------
void LCD_SetCursor(uint16_t x,uint16_t y)
{
  LCD_Write_REG(0x004E,x);
  LCD_Write_REG(0x004F,y);
  LCD_Write_Command(0x22);
}
//-----------------------------------------------------------------------------
//                     Прорисовка точки на ЖКИ
// x,y - координаты точки
// Color - цвет точки
//-----------------------------------------------------------------------------
void LCD_SetPoint(uint16_t x,uint16_t y,uint16_t Color)
{
  LCD_SetCursor(y,x);

  LCD_Clr_CS;                               /*   CS\   */
  LCD_Write_Command(0x22);
  LCD_Set_RS;                               /*   RS/   */
  LCD_Write_Data(Color);
  LCD_Clr_WR;                               /*   WR\   */
  LCD_Set_WR;                               /*   WR/   */
  LCD_Set_CS;                               /*   CS/   */
}

//-----------------------------------------------------------------------------
//                     чтение точки на ЖКИ
// x,y - координаты точки
// Color - цвет точки
//-----------------------------------------------------------------------------
uint16_t LCD_GetPoint(uint16_t x,uint16_t y)
{
  uint16_t cl;
  LCD_SetCursor(y,x);

  LCD_Clr_CS;                               /*   CS\   */
  LCD_Write_Command(0x22);
  LCD_Set_RS;                               /*   RS/   */
  LCD_Clr_RD;                               /*   WR\   */
  cl = LCD_Read_Data();
  LCD_Set_RD;                               /*   WR/   */
  LCD_Clr_RD;                               /*   WR\   */
  cl = LCD_Read_Data();
  LCD_Set_RD;                               /*   WR/   */
  LCD_Set_CS;                               /*   CS/   */
  return cl;
}
//-----------------------------------------------------------------------------
//                       Заливка экрана одним цветом
//-----------------------------------------------------------------------------
void LCD_FillScreen(uint16_t Color)
{
  LCD_SetArea(0, 0, 239, 319);

  LCD_Clr_CS;                             /*   CS\   */

  LCD_Set_RS;                             /*   RS/   */

  GPIOE->ODR  = Color;  // Загрузка данных в шину ЖКИ

  for(int i=0; i < 76800; i++)
  {
    LCD_Clr_WR;                           /*   WR\   */
    LCD_Set_WR;                           /*   WR/   */
  }
  LCD_Set_CS;                             /*   CS/   */
}
//-----------------------------------------------------------------------------
//                       Выбор зоны для рисования
//-----------------------------------------------------------------------------
void LCD_SetArea(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2)
{
  LCD_Write_Command(0x44); LCD_Write_Data((x2 << 8) | x1);    // Source RAM address window
  LCD_Write_Command(0x45); LCD_Write_Data(y1);    // Gate RAM address window
  LCD_Write_Command(0x46); LCD_Write_Data(y2);    // Gate RAM address window
  LCD_SetCursor(x1, y1);
}
//-----------------------------------------------------------------------------
//                       Вывод на ЖКИ символа шрифтом 5х7 пикселей
// x,y - начальные координаты области прорисовки символа
// c - сам символ
// t_color - цвет символа
// b_color - цвет фона
// rot - поворот символа : переворачивается, если не равен 0
// zoom - множитель, увеличивающий шрифт в целое число раз, начиная с 1
//-----------------------------------------------------------------------------
void LCD_WriteChar5x7(uint16_t x, uint16_t y, char c, uint16_t t_color, uint16_t b_color, uint8_t rot, uint8_t zoom )
{
  unsigned char h,ch,p,mask,z,z1;

  if (rot != 0) LCD_SetArea(x, y, x+(6*zoom)-1, y+(8*zoom)-1);
  else   	LCD_SetArea(y, x, y+(8*zoom)-1, x+(6*zoom)-1);

  for (h=0; h<6; h++)
    {
      if(h < 5)
        {
          if(c < 129) ch=font_5x7[ c-32 ][h];
          else        ch=font_5x7[ c-32-63 ][h];

          if (rot != 0)
            {
              LCD_Write_REG(0x0011,0x6078);
              LCD_Write_Command(0x22);
            }
        }
      else ch = 0;

      z1 = zoom;
      while(z1 != 0)
        {
          if (rot != 0) mask=0x01;
          else mask=0x80;

          for (p=0; p<8; p++)
            {
              z = zoom;
              while(z!=0)
                {
                  if (ch&mask) LCD_Write_Data(t_color);
                  else  LCD_Write_Data(b_color);

                  z--;
                }
              if (rot != 0) mask=mask<<1;
              else mask=mask>>1;
            }
          z1--;
	}
     }
}
//-----------------------------------------------------------------------------
//                       Вывод на ЖКИ символа шрифтом 8х16 пикселей
// x,y - начальные координаты области прорисовки символа
// c - сам символ
// t_color - цвет символа
// b_color - цвет фона
//-----------------------------------------------------------------------------
void LCD_WriteChar_8x16(uint16_t x, uint16_t y, char c, uint16_t t_color, uint16_t b_color)
{
  unsigned char tmp_char=0;

  for (uint8_t i=0;i<16;i++)
  {
    tmp_char=font_8x16[((c-0x20)*16)+i];
    for (uint8_t j=0;j<8;j++)
      {
        if ( (tmp_char >> 7-j) & 0x01 == 0x01) LCD_SetPoint(x+j,y+i,t_color); // Цвет символа
        else LCD_SetPoint(x+j,y+i,b_color); // Цвет фона
      }
  }
}
//-----------------------------------------------------------------------------
//                       Вывод на ЖКИ строки шрифтом 8x16 пикселей
// x,y - начальные координаты области прорисовки текста
// *text - сам текст
// t_color - цвет символа
// b_color - цвет фона
//-----------------------------------------------------------------------------
void LCD_WriteString_8x16(uint16_t x, uint16_t y, char *text, uint16_t charColor, uint16_t bkColor)
{
  for (uint8_t i=0; *text; i++) LCD_WriteChar_8x16((x+8*i), y, *text++, charColor, bkColor);
}
//-----------------------------------------------------------------------------
//                       Вывод на ЖКИ строки шрифтом 5х7 пикселей
// x,y - начальные координаты области прорисовки текста
// *text - сам текст
// t_color - цвет символа
// b_color - цвет фона
// rot - поворот текста : переворачивается, если не равен 0
// zoom - множитель, увеличивающий шрифт в целое число раз, начиная с 1
//-----------------------------------------------------------------------------
void LCD_WriteString_5x7(uint16_t x, uint16_t y, char *text, uint16_t charColor, uint16_t b_color, uint8_t rot, uint8_t zoom)
{
  for (uint8_t i=0; *text; i++) LCD_WriteChar5x7(x+(i*6*zoom),y,*text++,charColor, b_color, rot, zoom);
}
//-----------------------------------------------------------------------------
//                       Вывод на ЖКИ линии
// (x1,y1) - координаты начала линии
// (x2,y2) - координаты конца линии
// color - цвет линии
//-----------------------------------------------------------------------------
void LCD_Draw_Line(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color)
{
  uint16_t x, y, dx, dy;
  if(y1==y2)
    {
      if(x1<=x2) x=x1;
      else
        {
          x=x2;
	  x2=x1;
	}

      while(x <= x2)
        {
          LCD_SetPoint(x,y1,color);
          x++;
  	}
      return;
    }

  else if(y1>y2) dy=y1-y2;
       else dy=y2-y1;

  if(x1==x2)
    {
      if(y1<=y2) y=y1;
      else
        {
          y=y2;
	  y2=y1;
	}

      while(y <= y2)
        {
          LCD_SetPoint(x1,y,color);
          y++;
  	}
      return;
    }
  
  else if(x1 > x2)
    {
      dx=x1-x2;
      x = x2;
      x2 = x1;
      y = y2;
      y2 = y1;
    }
       else
         {
           dx=x2-x1;
           x = x1;
           y = y1;
 	 }
  if(dx == dy)
    {
      while(x <= x2)
        {
          x++;
          if(y>y2) y--;

          else y++;
          LCD_SetPoint(x,y,color);
  	}
    }
  else
    {
      LCD_SetPoint(x, y, color);
      if(y < y2)
        {
          if(dx > dy)
            {
              uint16_t p = dy * 2 - dx;
              uint16_t twoDy = 2 * dy;
              uint16_t twoDyMinusDx = 2 * (dy - dx);
              while(x < x2)
                {
                  x++;
                  if(p < 0) p += twoDy;
                  else
                    {
                      y++;
                      p += twoDyMinusDx;
     		    }
                  LCD_SetPoint(x, y,color);
    		}
   	    }
          else
            {
              uint16_t p = dx * 2 - dy;
              uint16_t twoDx = 2 * dx;
              uint16_t twoDxMinusDy = 2 * (dx - dy);
              while(y < y2)
                {
                  y++;
                  if(p < 0) p += twoDx;
                  else
                    {
                      x++;
      		      p+= twoDxMinusDy;
     		    }
                  LCD_SetPoint(x, y, color);
    		}
   	    }
  	}
      else
        {
          if(dx > dy)
            {
              uint16_t p = dy * 2 - dx;
              uint16_t twoDy = 2 * dy;
              uint16_t twoDyMinusDx = 2 * (dy - dx);
              while(x < x2)
                {
                  x++;
                  if(p < 0) p += twoDy;
                  else
                    {
                      y--;
                      p += twoDyMinusDx;
                    }
                  LCD_SetPoint(x, y,color);
    		}
   	     }
          else
            {
              uint16_t p = dx * 2 - dy;
              uint16_t twoDx = 2 * dx;
              uint16_t twoDxMinusDy = 2 * (dx - dy);
              while(y2 < y)
                {
                  y--;
                  if(p < 0) p += twoDx;
                  else
                    {
                      x++;
                      p+= twoDxMinusDy;
                    }
                  LCD_SetPoint(x, y,color);
    		}
   	     }
  	 }
      }
}
//-----------------------------------------------------------------------------
//                       Вывод на ЖКИ окружности
// (cx,cy) - координаты центра окружности
// color - цвет окружности
// fill - закрашивает круг, если !== 0
//-----------------------------------------------------------------------------
void LCD_Draw_Circle(uint16_t cx,uint16_t cy,uint16_t r,uint16_t color,uint8_t fill)
{
  uint16_t x=0,y=r;
  uint16_t delta=3-(r<<1),tmp;
  
  while(y>x)
    {
      if(fill)
        {
          LCD_Draw_Line(cx+x,cy+y,cx-x,cy+y,color);
          LCD_Draw_Line(cx+x,cy-y,cx-x,cy-y,color);
          LCD_Draw_Line(cx+y,cy+x,cx-y,cy+x,color);
          LCD_Draw_Line(cx+y,cy-x,cx-y,cy-x,color);
        }
      else
        {
          LCD_SetPoint(cx+x,cy+y,color);
          LCD_SetPoint(cx-x,cy+y,color);
          LCD_SetPoint(cx+x,cy-y,color);
          LCD_SetPoint(cx-x,cy-y,color);
          LCD_SetPoint(cx+y,cy+x,color);
          LCD_SetPoint(cx-y,cy+x,color);
          LCD_SetPoint(cx+y,cy-x,color);
          LCD_SetPoint(cx-y,cy-x,color);
        }
      x++;
      if(delta>=0)
        {
          y--;
          tmp=(x<<2);
          tmp-=(y<<2);
          delta+=(tmp+10);
        }
      else delta+=((x<<2)+6);
     }
}

//-----------------------------------------------------------------------------
//                       Вывод на ЖКИ окружности
// (Xc,Yc) - координаты центра окружности
// color - цвет окружности

//-----------------------------------------------------------------------------

void Lcd_Circle(uint16_t  Xc, uint16_t Yc, uint16_t r, uint16_t color) {
  uint16_t x,y,d;
  d = 3 - (r*2);
  x = 0;
  y = r;
  while(x<=y) {
   LCD_SetPoint(Xc+x,Yc+y, color);
   LCD_SetPoint(Xc+x,Yc-y, color);

   LCD_SetPoint(Xc-x,Yc+y, color);
   LCD_SetPoint(Xc-x,Yc-y, color);
   LCD_SetPoint(Xc+y,Yc+x, color);
   LCD_SetPoint(Xc+y,Yc-x, color);
   LCD_SetPoint(Xc-y,Yc+x, color);
   LCD_SetPoint(Xc-y,Yc-x, color);

   if(d<0) {
     d = d + (x*4) + 6;
   }
   else {
     d = d + ((x-y)*4) + 10;
     y--;
   }
   x++;
  }
}

//-----------------------------------------------------------------------------
//                     Вывод на ЖКИ прямоугольника
//  (x1,y1) - координаты левого нижнего угла прямоугольника
//  (x2,y2) - координаты правого верхнего угла прямоугольника
//  color - цвет прямоугольника
//  fill - закрашивает прямоугольник, если !== 0
//-----------------------------------------------------------------------------
void LCD_Draw_Rectangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2,uint16_t color,uint8_t fill)
{
  if(fill)
    {
      uint16_t i;
      if(x1>x2)
        {
          i=x2;
          x2=x1;
        }
      else i=x1;
      for(;i<=x2;i++) LCD_Draw_Line(i,y1,i,y2,color);
      return;
    }
  LCD_Draw_Line(x1,y1,x1,y2,color);
  LCD_Draw_Line(x1,y2,x2,y2,color);
  LCD_Draw_Line(x2,y2,x2,y1,color);
  LCD_Draw_Line(x2,y1,x1,y1,color);
}
//-----------------------------------------------------------------------------
//                    Вывод на ЖКИ рисунка из массива
// (x0,y0) - координаты левого нижнего угла рисунка
// *str - массив рисунка
//-----------------------------------------------------------------------------
void LCD_Draw_Picture(uint16_t x0, uint16_t y0, const unsigned char *str)
{
  uint32_t temp, i;
  uint16_t y1, imageWidth, imageHeight;
  ColorTypeDef color;

  color.uint8_t[1] =*(unsigned short *)(&str[ 0]);
  color.uint8_t[0]=*(unsigned short *)(&str[ 1]);
  imageWidth = color.uint16_t;
 // x1 = imageWidth + x0;

  color.uint8_t[1] =*(unsigned short *)(&str[ 2]);
  color.uint8_t[0]=*(unsigned short *)(&str[ 3]);
  imageHeight = color.uint16_t;
  y1 = imageHeight + y0;

  temp = 2;
  while (y0<=y1-1)
    {
      LCD_SetCursor(x0, y0);
      LCD_Clr_CS;              /*   CS\   */
      LCD_Clr_RS;              /*   RS\   */
      LCD_Set_RD;              /*   RD/   */
      LCD_Write_Data(0x0022);
      LCD_Clr_WR;              /*   WR\   */
      LCD_Set_WR;              /*   WR/   */
      LCD_Set_RS;              /*   RS/   */
      for(i=0;i<=imageWidth-1;i++)
        {  //Write all pixels to ram, and then update display (fast)
          color.uint8_t[1] =*(unsigned short *)(&str[ 2 * temp]);
	  color.uint8_t[0]=*(unsigned short *)(&str[ 2 * temp+1]);
          LCD_Write_Data(color.uint16_t);
          LCD_Clr_WR;
          LCD_Set_WR;
          temp++;
        }
      y0++;
      LCD_Set_CS;     /*   CS/   */
    }
}
void LCD_Draw_Picture2(uint16_t x0, uint16_t y0, const uint16_t *img)
{
	const uint16_t *ptr = img;
	int imageWidth = *ptr++;
	int imageHeight = *ptr++;
	int y1, i;

	for (y1 = imageHeight+y0; y0 < y1; ++y0) {
		LCD_SetCursor(x0, y0);
		LCD_Clr_CS;				/*   CS\   */
		//LCD_Clr_RS;			/*   RS\   */
		//LCD_Set_RD;			/*   RD/   */
		LCD_Write_Command(0x22);
		//LCD_Clr_WR;			/*   WR\   */
		//LCD_Set_WR;			/*   WR/   */
		LCD_Set_RS;				/*   RS/   */

		for (i = imageWidth; i--; ) {
			LCD_Write_Data(*ptr++);
		}

		LCD_Clr_WR;
		LCD_Set_WR;
		LCD_Set_CS;				/*   CS/   */
	}
}
//-----------------------------------------------------------------------------
//                    Вывод на ЖКИ BMP из массива 16 bit
// (x0,y0) - координаты левого нижнего угла рисунка
// *str - массив рисунка
//-----------------------------------------------------------------------------
void LCD_Draw_BMP(uint16_t x0, uint16_t y0, const unsigned int *str, uint8_t clck)
{
  uint32_t temp, i;
  uint16_t y1, imageWidth, imageHeight;
  uint16_t color;
  imageWidth = *(unsigned int *)(&str[0]);
  //x1 = imageWidth + x0;
  imageHeight = *(unsigned int *)(&str[ 1]);
  y1 = imageHeight + y0;

  if (!clck){
  temp = 2;
  while (y0<=y1-1)
    {
      for(i=0;i<=imageWidth-1;i++)
        {
          color =*(unsigned int *)(&str[temp]);
	  LCD_SetPoint(x0+i,y1,color);
          temp++;
        }
      y1--;
     }
  } else {
   temp = (imageWidth*imageHeight)+2;
  while (y0<=y1-1)
    {
      for(i=0;i<=imageWidth-1;i++)
        {
          color =*(unsigned int *)(&str[temp]);
	  LCD_SetPoint(x0+i,y1,color);
          temp++;
        }
      y1--;
     }
}
}

//-----------------------------------------------------------------------------
